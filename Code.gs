function getAllUrlsByHeading() {
  var results = {};
  var doc = DocumentApp.getActiveDocument();
  var body = doc.getBody();

  body.appendPageBreak();

  var elements = body.getNumChildren();
  var heading;
  for( var i=0;i<elements;i++) {
    var element = body.getChild(i).copy();
    var type = element.getType();
    // Set the heading if present
    if( type == DocumentApp.ElementType.PARAGRAPH ){
      paragraphHeading = element.asParagraph().getHeading();
      if( paragraphHeading !== DocumentApp.ParagraphHeading.NORMAL ){
        heading = element.getText();
        results[heading] = [];
      }
    }
    // Retrieve all links
    // https://stackoverflow.com/a/76335465
    // Convert the element to a Text element
    let text = element.asText();

    // Get all text attributes
    let indices = text.getTextAttributeIndices();

    indices.forEach(index => {
      // Get URL at x
      let url = text.getLinkUrl(index);
      // If it found an URL add it to the list
      if (url) {
        results[heading].push(url);
      }
    });
  }
  return results;
}

function filterDuplicatesInArray(arr) {
  return arr.filter((currentValue, currentIndex) => arr.indexOf(currentValue) !== currentIndex);
}

function getDuplicateUrls(urlsByHeading, urlSanitizer, duplicates) {
  var result = {};
  for (const heading in urlsByHeading) {
    var urlsForHeading = urlsByHeading[heading].map(url => urlSanitizer(url));
    var duplicatesForHeading = urlsForHeading.filter(url => duplicates.includes(url));
    if(duplicatesForHeading.length !== 0) {
      result[heading] = duplicatesForHeading;
    }
  }
  return result;
}

function getDuplicateUrlsWithExactMatch() {
  const urlsByHeading = getAllUrlsByHeading();
  const flattenedUrls = Object.values(urlsByHeading).flat();
  const urlSanitizer = url => url
  const duplicates = filterDuplicatesInArray(flattenedUrls).map(url => urlSanitizer(url));
  return getDuplicateUrls(urlsByHeading, urlSanitizer, duplicates)
}

function getDuplicateUrlsWithIgnoreFragment() {
  const urlsByHeading = getAllUrlsByHeading();
  const flattenedUrls = Object.values(urlsByHeading).flat();
  const urlSanitizer = url => {
    // const urlObj = new URL(url);
    // urlObj.hash = '';
    // return urlObj.toString();
    return url.split('#')[0];
  }
  const duplicates = filterDuplicatesInArray(flattenedUrls).map(url => urlSanitizer(url));
  return getDuplicateUrls(urlsByHeading, urlSanitizer, duplicates)
}

function onOpen() {
  DocumentApp.getUi()
    .createMenu('URL Duplication Details')
    .addItem('List all URLs', 'listAllURLsSidebar')
    .addItem('List duplicate URLs with exact match', 'listDuplicateURLsWithExactMatchSidebar')
    .addItem('List duplicate URLs with ignore fragment', 'listDuplicateURLsWithIgnoreFragmentSidebar')
    .addToUi();
}

function showSidebarInUI(urls) {
  var template = HtmlService.createTemplateFromFile('Page');
  // template.data = JSON.stringify(
  //   urls,
  //   null,
  //   // '\u{0020}'.repeat(2)
  // );
  template.urls = urls;
  var html = template.evaluate()
  DocumentApp.getUi() // Or DocumentApp or SlidesApp or FormApp.
    .showSidebar(html);
}

function listAllURLsSidebar() {
  return showSidebarInUI(getAllUrlsByHeading())
}

function listDuplicateURLsWithExactMatchSidebar() {
  return showSidebarInUI(getDuplicateUrlsWithExactMatch())
}

function listDuplicateURLsWithIgnoreFragmentSidebar() {
  return showSidebarInUI(getDuplicateUrlsWithIgnoreFragment())
}
