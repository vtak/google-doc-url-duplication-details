# google-doc-url-duplication-details

This project helps you perform the following actions in a Google Docu using Apps Script.
- Get a list of all URLs in the document
- Get a list of duplicated URLs in the document
- Get a list of duplicated URLs in the document while ignoring the fragment part of the URL

## Setup

1. Go to the Google Doc you want to get these details for.
1. Setup Apps Script for the Google Doc by clicking on `Extensions > Apps Scripts`.
1. Add Apps Script project name on the top menu bar. Let's call it `URL Duplication Details`.
1. Add the following files from this project to the Apps Script project and save them from the left sidebar -
  1. Click on `File > + > Script` and name it `Code`. Copy the content from `Code.gs` file in this project.
  1. Click on `File > + > HTML` and name it `Page`. Copy the content from `Page.html` file in this project.
1. Go to Google Doc and refresh it.
1. Wait for the `URL Duplication Details` menu item to show up in the Google Doc top menu and click on 
  1. `URL Duplication Details > List all URLs` - Get a list of all URLs in the document
  1. `URL Duplication Details > List duplicate URLs with exact match` - Get a list of duplicated URLs in the document
  1. `URL Duplication Details > List duplicate URLs with ignore fragment` - Get a list of duplicated URLs in the document while ignoring the fragment part of the URL

__Note__ : For some odd reason, running the scripts, adds a newline character at the end of the document. Please ensure you delete the extra characters at the end of the document to keep it clean.
